﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;
CREATE TABLE user(
 id SERIAL PRIMARY KEY NOT NULL,
 login_id varchar(255) UNIQUE NOT NULL,
 name varchar(255) NOT NULL,
 birth_date DATE NOT NULL,
 password varchar(255) NOT NULL,
 is_admin boolean DEFAULT FALSE NOT NULL,
 create_date DATETIME NOT NULL,
 update_date DATETIME NOT NULL
);
INSERT INTO user(
 login_id,
 name,
 birth_date,
 password,
 is_admin,
 create_date,
 update_date
) 
VALUES(
 'admin',
 '管理者',
 '2022-10-13',
 'password',
 true,
 now(),
 now()
);
USE usermanagement;
INSERT INTO user(
 login_id,
 name,
 birth_date,
 password,
 create_date,
 update_date
)
VALUES(
 'user1',
 '一般1',
 '2022-10-19',
 'password',
 now(),
 now()
);

USE usermanagement;
INSERT INTO user(
 login_id,
 name,
 birth_date,
 password,
 create_date,
 update_date
)
VALUES(
 'user2',
 '一般2',
 '2022-10-19',
 'password',
 now(),
 now()
);

INSERT INTO user(
 login_id,
 name,
 birth_date,
 password,
 create_date,
 update_date
)
VALUES(
 'user3',
 '一般3',
 '2022-10-19',
 'password',
 now(),
 now()
);
USE usermanagement;
DELETE FROM user WHERE id=5;

USE usermanagement;
UPDATE user SET password ='5F4DCC3B5AA765D61D8327DEB882CF99' WHERE id = 1;

SELECT * FROM user WHERE login_id =? and password = ?;

USE usermanagement;
SELECT * FROM user WHERE is_admin = false AND name LIKE'%一般%';

SELECT * FROM user WHERE is_admin = false;

SELECT * FROM user WHERE
 is_admin = false AND
 name = 'yamamotomao' AND 
 birth_date >= '1996-02-01' AND 
 birth_date <= '2022-01-01';

SELECT * FROM user WHERE 
 is_admin = false AND
 login_id = 'user1';

SELECT * FROM user WHERE
 is_admin = false AND 
 is_admin = false AND 
 login_id = 'user1' AND 
 name = 'yamamotomao' AND 
 birth_date >= '1996-02-01' AND 
 birth_date <= '2022-01-01';
 
SELECT * FROM user WHERE 
 login_id = 'user1' AND 
 name = 'yamamotomao' AND 
 birth_date >= '1996-02-01 00:00:00';



