package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      response.sendRedirect("LoginServlet");
      return;

    }
    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);

    UserDao userDao = new UserDao();
    User user = userDao.findById(id);

    request.setAttribute("user", user);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");

    String password = request.getParameter("password");
    String confirmPassword = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String strBirthday = request.getParameter("birth-date");
    String loginId = request.getParameter("user-loginid");
    String strId = request.getParameter("user-id");
    int id = Integer.valueOf(strId);
    Date birthday = null;
    if (!strBirthday.equals("")) {
      birthday = Date.valueOf(strBirthday);
    }

    if (!password.equals(confirmPassword) || name.equals("") || strBirthday.equals("")) {
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      User user = new User();
      user.setId(id);
      user.setLoginId(loginId);
      user.setName(name);
      user.setBirthDate(birthday);


      request.setAttribute("user", user);


      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);


      return;

    }

    if (password.equals("")) {
      UserDao userDao = new UserDao();
      userDao.update(name, strBirthday, id);

    } else {

      PasswordEncorder passwordEncorder = new PasswordEncorder();
      String encordPassword = passwordEncorder.encordPassword(password);

      UserDao userDao = new UserDao();
      userDao.updatePassword(name, encordPassword, strBirthday, id);

    }
    response.sendRedirect("UserListServlet");

  }
}

