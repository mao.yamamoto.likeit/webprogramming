package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */


  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    // request.setCharacterEncoding("UTF-8");

    // response.getWriter().append("Served at: ").append(request.getContextPath());

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;

    }

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");

        String loginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String birthday = request.getParameter("birth-date");

        UserDao userDao = new UserDao();


        if (!userDao.findByLoginId(loginId) || !password.equals(confirmPassword)
            || loginId.equals("")
            || password.equals("") || confirmPassword.equals("") || name.equals("")
            || birthday.equals("")) {
          request.setAttribute("errMsg", "入力された内容は正しくありません");
          request.setAttribute("loginId", loginId);
          request.setAttribute("name", name);
          request.setAttribute("birthday", birthday);

          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
          dispatcher.forward(request, response);

          return;
        }
        
        PasswordEncorder passwordEncorder = new PasswordEncorder();
        String encordPassword = passwordEncorder.encordPassword(password);
        
        userDao.insert(loginId, name, encordPassword, birthday);


        response.sendRedirect("UserListServlet");


        // doGet(request, response);
	}

}
