<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ユーザ一覧画面</title>
	<!-- BootstrapのCSS読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
		crossorigin="anonymous">
	<!-- オリジナルCSS読み込み -->
	<link href="css/original/common.css" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
			<div class="container">
				<a class="navbar-brand" href="UserListServlet">ユーザ管理システム</a>

				<div class="collapse navbar-collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><span class="navbar-text">
								${userInfo.name}</span><span class="navbar-text">さん</span></li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="text-center">ユーザ一覧</h1>
			</div>
		</div>
		

		<!-- 新規登録リンク -->
		<div class="container">
		<div class="row mb-3">
			<div class="col text-right">
				<a href="UserAddServlet">新規登録</a>
			</div>
		</div>


		<!-- 検索ボックス -->
		<div class="row">
			<div class="col">
				<form action="UserListServlet" method="post">
				<div class="card">
					<div class="card-body">

						<div class="form-group row">
							<label for="loginId" class="col-2 col-form-label">ログインID</label>
							<div class="col-10">
								<input type="text" name="user-loginid" class="form-control" id="loginId" value="">
							</div>
						</div>

						<div class="form-group row">
							<label for="userName" class="col-2 col-form-label">ユーザ名</label>
							<div class="col-10">
								<input type="text" name="user-name" class="form-control" id="userName" value="">
							</div>
						</div>

						<div class="form-group row">
							<label for="birthDate" class="col-2 col-form-label">生年月日</label>
							<div class="col-10">
								<div class="row">
									<div class="col-5">
										<input type="date" name="date-start" id="startBirthDate" class="form-control"
											value="">
									</div>
									<div class="col-2 text-center">~</div>
									<div class="col-5">
										<input type="date" name="date-end" id="endBirthDate" class="form-control"
											value="">
									</div>
								</div>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary form-submit">検索</button>
						</div>
					</div>
				</form>
			</div>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<!-- TODO 未実装；ログインボタンの表示制御を行う -->
							   <td class="float-right">
							   <a class="btn btn-primary"
								href="UserDetailServlet?id=${user.id}">詳細</a>							
								
								<c:if test="${userInfo.admin or userInfo.loginId == user.loginId}">
								<a class="btn btn-success" 
								href="UserUpdateServlet?id=${user.id}">更新</a></c:if>
								
								<c:if test="${userInfo.admin}">
							    <a class="btn btn-danger"
								href="UserDeleteServlet?id=${user.id}">削除</a></c:if>
								</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
