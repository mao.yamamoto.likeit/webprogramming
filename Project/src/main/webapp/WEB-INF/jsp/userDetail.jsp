<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja"></html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ詳細 画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
			<div class="container">
				<a class="navbar-brand" href="UserListServlet">ユーザ管理システム</a>

				<div class="collapse navbar-collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><span class="navbar-text">
								${userInfo.name}</span><span class="navbar-text">さん</span></li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</div>
		</nav>

	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ情報詳細参照</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">

				<div class="row">
					<label for="loginId" class="col-3 font-weight-bold">ログインID</label>
					<div class="col-9">
						<p>${user.loginId}</p>
					</div>
				</div>

				<div class="row">
					<label for="userName" class="col-3 font-weight-bold">ユーザ名</label>
					<div class="col-9">
						<p>${user.name}</p>
					</div>
				</div>

				<div class="row">
					<label for="birthDate" class="col-3 font-weight-bold">生年月日</label>
					<div class="col-9">
						<p>${user.birthDate}</p>
					</div>
				</div>

				<div class="row">
					<label for="createDate" class="col-3 font-weight-bold">新規登録日時</label>
					<div class="col-9">
						<p>${user.createDate}</p>
					</div>
				</div>

				<div class="row">
					<label for="updateDate" class="col-3 font-weight-bold">更新日時</label>
					<div class="col-9">
						<p>${user.updateDate}</p>
					</div>
				</div>


				<div class="col-xs-4">
					<a href="UserListServlet">戻る</a>
				</div>
			</div>


		</div>
	</div>
</body>

</html>